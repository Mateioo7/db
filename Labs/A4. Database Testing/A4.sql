use Zoo
go
select * from Tests

insert into Tables(Name) values
	('Food'),       -- 1 pk + 0 fk
	('Department'), -- needed to insert in table Animal
	('Animal'),     -- 1 pk + 1 fk
	('AnimalFood'); -- 2 pk

insert into Tests(Name) values
	('selectViewFood'), ('selectViewDepartmentsWithMoreThan20Animals'),
	('selectViewAnimalsAndCorrespondingFood'), 
	('selectViewMaxAnimalAgeForDepartmentsGroupedByNoOfAnimals'),
	('insertFood'), ('deleteFood'),
	('insertDepartment'), ('deleteDepartment'),
	('insertAnimal'), ('deleteAnimal'),
	('insertAnimalFood'), ('deleteAnimalFood');

insert into Views(Name) values
	('viewFood'),
	('viewDepartmentsWithMoreThan20Animals'),
	('viewAnimalsAndCorrespondingFood'),
	('viewMaxAnimalAgeForDepartmentsGroupedByNoOfAnimals');

insert into TestViews(TestID, ViewID) values
	(1, 1), (2, 2), (3, 3), (4, 4);

delete from TestTables
-- noOfRows = no. of records to insert / delete
-- position = order of execution
insert into TestTables(TestId, TableId, NoOfRows, Position) values
	(12, 4, 1000, 1), -- testId 12: deleteAnimalFood, tableId 4: AnimalFood
	(10, 3, 1000, 2), -- testId 10: deleteAnimal, tableId 3: Animal
	(8, 2, 1000, 3), -- testId 8: deleteDepartment, tableId 2: Department
	(6, 1, 1000, 4), -- testId 6: deleteFood, tableId 1: Food
	(5, 1, 1000, 5), -- testId 5: insertFood, tableId 1: Food
	(7, 2, 1000, 6), -- testId 7: insertDepartment, tableId 2: Department
	(9, 3, 1000, 7), -- testId 9: insertAnimal, tabledId 3: Animal
	(11, 4, 1000, 8); -- testId 11: insertAnimalFood, tabledId 4: AnimalFood

create or alter procedure insertFood
	@rowsToOperate int
as
	declare @index int = 1

	while @index <= @rowsToOperate
	begin
		insert into Food (FId, Name, Kilos, PricePerKilo) values 
			(@index, CONVERT(varchar(255), NEWID()), ABS(CHECKSUM(NEWID()) % 51) + 10, 
			ABS(CHECKSUM(NEWID()) % 101) + 20)
		set @index = @index + 1
	end
go

create or alter procedure deleteFood
	@rowsToOperate int
as 
	execute('deleteFromTable Food' + ', ' + @rowsToOperate); 
go

create or alter procedure insertDepartment
	@rowsToOperate int
as
	declare @index int = 1
	declare @noOfSpecies int
	declare @noOfAnimals int

	while @index <= @rowsToOperate
	begin
		set @noOfSpecies = ABS(CHECKSUM(NEWID()) % 11) + 3
		set @noOfAnimals = @noOfSpecies * 2 + ABS(CHECKSUM(NEWID()) % 11)
		insert into Department (DId, Name, Director, NoOfSpecies, NoOfAnimals) values
			(@index, CONVERT(varchar(255), NEWID()), CONVERT(varchar(255), NEWID()), 
			@noOfSpecies, @noOfAnimals)
		set @index = @index + 1
	end
go

create or alter procedure deleteDepartment
	@rowsToOperate int
as 
	execute('deleteFromTable Department' + ', ' + @rowsToOperate);  
go

create or alter procedure insertAnimal
	@rowsToOperate int
as
	declare @index int = 1, @gender varchar, @departmentId int
	select @departmentId = min(DId) from Department

	while @index <= @rowsToOperate
	begin
		set @gender = (CASE WHEN ABS(CHECKSUM(NEWID()) % 2) = 1 THEN 'M' ELSE 'F' END)
		insert into Animal (AId, Species, Name, Gender, Age, DId) values
			(@index, CONVERT(varchar(255), NEWID()), CONVERT(varchar(255), NEWID()), @gender,
			ABS(CHECKSUM(NEWID()) % 21) + 1, @departmentId)
		set @index = @index + 1
	end
go

create or alter procedure deleteAnimal 
	@rowsToOperate int
as 
	execute('deleteFromTable Animal' + ', ' + @rowsToOperate); 
go

delete from AnimalFood
exec insertAnimalFood 1000

create or alter procedure insertAnimalFood
	@rowsToOperate int
as
	declare @index int = 1, @month varchar(30), @randomIndex int, @animalIndex int, @foodIndex int
	select @animalIndex = min(AId) from Animal

	while @index <= @rowsToOperate
	begin
		select @foodIndex = min(FId) from Food where FId >= @index
		set @randomIndex = ABS(CHECKSUM(NEWID()) % 12) + 1
		select @month = choose(@randomIndex, 'January', 'February', 'March', 'April', 'May',
			'June', 'July', 'August', 'September', 'October', 'November', 'December')

		insert into AnimalFood (AId, FId, Month, Year) values
			(@animalIndex, @foodIndex, @month, ABS(CHECKSUM(NEWID()) % 21) + 2000)
		set @index = @index + 1
	end
go

create or alter procedure deleteAnimalFood
	@rowsToOperate int
as 
	execute('deleteFromTable AnimalFood' + ', ' + @rowsToOperate); 
go

select * from AnimalFood
exec insertAnimalFood 1000
exec deleteFromTable 'AnimalFood', 2000

create or alter procedure deleteFromTable 
	@tableName varchar(30), @rowsToOperate int
as 
	declare @rowsDeleted int = 0
	
	while @rowsDeleted < @rowsToOperate
	begin
		execute('delete top(1) from ' + @tableName)
		set @rowsDeleted = @rowsDeleted + 1
	end
go

--set @sql = N'select @recordsLeftInTable = count(*) from ' + @tableName
--execute sp_executesql @sql, N'@recordsLeftInTable int output', @recordsLeftInTable output

-- select on 1 table
create or alter view viewFood as select * from Food go

create or alter view viewDepartmentsWithMoreThan20Animals
as 
	select * from Department D where D.NoOfAnimals > 20
go

-- select on 2 tables
create or alter view viewAnimalsAndCorrespondingFood
as 
	select A.AId, A.Species, A.Gender, A.Age, AF.FId, AF.Month, AF.Year from Animal A 
		left join AnimalFood AF on A.AId = AF.AId
go

select * from Department D
select * from Animal

-- select on 2 tables with a group by clause
create or alter view viewMaxAnimalAgeForDepartmentsGroupedByNoOfAnimals
as
	select D.NoOfAnimals, max(A.Age) as 'MaximumAnimalAge' from Department D
		inner join Animal A on D.DId = A.DId
			group by D.NoOfAnimals
go

create or alter procedure runViewTests
as
	declare @currentViewId int = 1, @numberOfViewTests int, @viewName varchar(100)
	select @numberOfViewTests = count(*) from TestViews

	while @currentViewId <= @numberOfViewTests
	begin
		select @viewName = V.Name from Views V where V.ViewID = @currentViewId
		execute('recordViewOperation ' + @viewName + ', ' + @currentViewId)

		set @currentViewId = @currentViewId + 1
	end
go

create or alter procedure recordViewOperation
	@viewName varchar(100), @viewId int
as
	declare @startTime datetime, @endTime datetime

	set @startTime = getdate()
	execute('select * from ' + @viewName)
	--print('executing select * from ' + @viewName + '...')
	set @endTime = getdate()

	insert into TestRuns(Description, StartAt, EndAt)
		values ('test_' + @viewName, @startTime, @endTime)
	insert into TestRunViews(TestRunID, ViewID, StartAt, EndAt)
		values (@@identity, @viewId, @startTime, @endTime)
go

create or alter procedure runTableTests
as
	declare @currentTableTestIndex int = 1, @numberOfTableTests int
	select @numberOfTableTests = count(*) from TestTables

	while @currentTableTestIndex <= @numberOfTableTests
	begin
		declare @testId int, @tableId int, @rowsToOperate int, @operation varchar(30), @s varchar(200)
		select @tableId = TT.TableID from TestTables TT where TT.Position = @currentTableTestIndex
		select @rowsToOperate = TT.NoOfRows from TestTables TT where TT.Position = @currentTableTestIndex
		select @testId = TT.TestID from TestTables TT where TT.Position = @currentTableTestIndex
		select @operation = T.Name from Tests T where T.TestID = @testId
		
		execute('recordTableOperation ' + @tableId + ', ' + @operation + ', ' + @rowsToOperate)

		set @currentTableTestIndex = @currentTableTestIndex + 1
	end
	/*
	select * from AnimalFood
	execute('recordTableOperation 4, deleteAnimalFood, 1000')
	execute('recordTableOperation 4, insertAnimalFood, 1000')
	*/
go

SET NOCOUNT ON;

exec runViewTests

exec runTableTests

select * from TestRuns

select * from TestRunViews
select * from TestRunTables

delete from TestRuns
dbcc checkident ('[TestRuns]', reseed, 0);

create or alter procedure recordTableOperation 
	@tableId int, @operation varchar(30), @rowsToOperate int
as
	declare @startTime datetime, @endTime datetime

	set @startTime = getdate()
	execute(@operation + ' ' + @rowsToOperate)
	print('executing ' + @operation + ' for ' + convert(varchar(10), @rowsToOperate) + ' records...')
	set @endTime = getdate()

	insert into TestRuns(Description, StartAt, EndAt)
		values ('test_' + @operation + '_' + convert(varchar(10), @rowsToOperate), @startTime, @endTime)
	insert into TestRunTables(TestRunID, TableID, StartAt, EndAt)
		values (@@identity, @tableId, @startTime, @endTime)
go