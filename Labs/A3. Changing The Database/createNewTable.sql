create procedure createNewTable
as
begin
	create table AnimalStable (
		ASId int not null,
		NoOfAnimals int,
		NoOfFemales int,
		NoOfMales int,
		constraint pk_AnimalStable primary key(ASId)
	);
end