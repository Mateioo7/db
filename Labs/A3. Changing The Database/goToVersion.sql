create procedure goToVersion @requestedVersion int
as
begin
	if @requestedVersion < 0 or @requestedVersion > 7
	begin
		declare @errorMessage1 varchar(70)
		set @errorMessage1 = 'Invalid given version: ' + cast(@requestedVersion as varchar) + '. Try a value within range [0, 7].'
		raiserror(@errorMessage1, 18, 1)
		return
	end

	declare @currentVersion int, @procedureName varchar(30), @procedureMessage varchar(100)
	select top 1 @currentVersion = Version from VersionTable

	if @currentVersion = @requestedVersion
	begin
		print 'The version is already ' + cast(@requestedVersion as varchar) + '. Try another value within range [0, 7].'
		return
	end

	print 'Changing version from ' + cast(@currentVersion as varchar) + ' to ' + cast(@requestedVersion as varchar) + '...'

	if @currentVersion < @requestedVersion -- we need to raise version <=> do stuff
	begin
		while @currentVersion < @requestedVersion
		begin
			set @currentVersion = @currentVersion + 1
			set @procedureName = case @currentVersion
									when 1 then 'modColType'
									when 2 then 'addNewCol'
									when 3 then 'addDefConstr'
									when 4 then 'createPriKey'
									when 5 then 'createUniqKey'
									when 6 then 'createForeKey'
									when 7 then 'createNewTable'
									end

			set @procedureMessage = case @currentVersion
										when 1 then '1. Column Director from the table Department is now of type char'
										when 2 then '2. Column Budget of type int created in table Department'
										when 3 then '3. Default constraint of 0 added to NoOfAnimals column from table Department'
										when 4 then '4. Primary key created in the table Cage'
										when 5 then '5. Unique key constraint created in the table Cage'
										when 6 then '6. Foreign key constraint created in the table Cage'
										when 7 then '7. Table AnimalStable created'
										end

			exec @procedureName
			print @procedureMessage
		end -- while end
	end -- if end

	if @currentVersion > @requestedVersion -- we need to lower version <=> undo stuff
	begin
		while @currentVersion > @requestedVersion
		begin
			set @procedureName = case @currentVersion
									when 1 then 'undoModColType'
									when 2 then 'undoAddNewCol'
									when 3 then 'undoAddDefConstr'
									when 4 then 'undoCreatePriKey'
									when 5 then 'undoCreateUniqKey'
									when 6 then 'undoCreateForeKey'
									when 7 then 'undoCreateNewTable'
									end

			set @procedureMessage = case @currentVersion
										when 1 then '1. Column Director from the table Department is again of type varchar'
										when 2 then '2. Column Budget of type int dropped from table Department'
										when 3 then '3. Default constraint of 0 dropped from NoOfAnimals column from table Department'
										when 4 then '4. Primary key dropped from the table Cage'
										when 5 then '5. Unique key constraint dropped from the table Cage'
										when 6 then '6. Foreign key constraint dropped from the table Cage'
										when 7 then '7. Table AnimalStable dropped'
										end

			exec @procedureName
			print @procedureMessage
			set @currentVersion = @currentVersion - 1
		end -- while end
	end -- if end
	
	update VersionTable
		set Version = @currentVersion
	where Version >= 0 or Version <= 7

	print 'Version updated to version ' + cast(@currentVersion as varchar)
end