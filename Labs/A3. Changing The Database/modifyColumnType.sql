USE [Zoo]
GO
/****** Object:  StoredProcedure [dbo].[modColType]    Script Date: 11/11/2020 4:21:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[modColType]
as
begin
	alter table Department
	alter column Director char(50)

	print 'Field Director is of type char now'
end