create table TableA (
	aId int identity (1,1),
	a2 int,
	a3 varchar(50),
	constraint pk_TA primary key (aId),
	constraint uk_TA unique(a2)
)

create table TableB (
	bId int identity (1,1),
	b2 int,
	b3 varchar(50),
	constraint pk_TB primary key (bId)
)

create table TableC (
	cId int primary key identity (1,1),
	aId int foreign key references TableA(aId),
	bId int foreign key references TableB(bId)
)

insert into TableA(a2, a3) values (23, 'fafafa'), (141, 'afauvfu'), (4188, 'afafubuvua')
insert into TableB(b2, b3) values (1, 'fu1qu'), (58, 'afyf	6mb'), (344, 'ggisghib')
insert into TableC(aId, bId) values (1, 2), (1, 3), (2, 3), (3, 1), (3, 3)

/*
primary key column -> clustered index created automatically
unique column -> nonclustered index created automatically

Index Scan (= Table Scan) retrieves all the rows from the table.
Index Seek retrieves selective rows from the table.

at where and join�s is very important the field(s) used in select clause (the field(s) decide which 
of the index is used).
at order by is very important the field(s) used in order by (the field(s) decide which of the index
is used).

a. Write queries on Ta such that their execution plans contain the following operators:
*/

-- clustered index scan
select * from TableA order by aId

-- clustered index seek
select * from TableA where aId > 100

-- nonclustered index scan + key lookup
select * from TableA order by a2

-- nonclustered index seek
select aId, a2 from TableA where a2 = 23


/*
b. Write a query on table Tb with a WHERE clause of the form
WHERE b2 = value and analyze its execution plan. Create a nonclustered
index that can speed up the query. Recheck the query's execution plan
(operators, SELECT's estimated subtree cost).
*/
if exists (select name from sys.indexes where name='n_index_TableB_b2')
	drop index n_index_TableB_b2 on TableB

-- n_index_TableB_b2 not created -> clustered index scan with estimated subtree cost: 0.0032853
select bId from TableB where b2 = 1234
-- n_index_TableB_b2 created -> nonclustered index seek with e.s.t.: 0.0032831
-- conclusion: the created noncl. index sped up the query

create nonclustered index n_index_TableB_b2 on TableB(b2)


/*
c. Create a view that joins at least 2 tables. Check whether existing
indexes are helpful; if not, reassess existing indexes / examine the
cardinality of the tables.
*/
create or alter view viewAC as
	select TA.aId, TA.a2, TC.cId
		from TableA TA inner join TableC TC on TA.aId = TC.aId
go

if exists (select name from sys.indexes where name='n_index_TableC_aId')
	drop index n_index_TableC_aId on TableC

-- noncl. index not created -> cl. index scan on TableC (0.0032875) + cl. index seek on TableA (0.0039155)
-- conclusion: both indexes can be used, but the cl. index from TableC.aId is more efficient (0.0032875)
select * from viewAC
-- noncl. index created -> noncl. index scan on TableA.a2 (0.0032853) + noncl. index seek on TableC.aId (0.0036015)
-- conclusion: both indexes can be used, but the noncl. index scan on TableA.a2 is more efficient (0.0032853)

-- therefore, the most efficient scan is is the noncl. index scan on TableA2 (0.0032853), so the creation of the
-- noncl. index on TableC.aId was beneficial

-- we create nonclustered index for the used foregin key
create nonclustered index n_index_TableC_aId on TableC(aId)