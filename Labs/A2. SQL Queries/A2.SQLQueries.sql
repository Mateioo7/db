-- insert data - for at least 4 tables; at least one statement should violate 
-- referential integrity constraints
insert into 
	Department(DId, Name, Director, NoOfSpecies, NoOfAnimals) 
values 
	(1, 'Big Cats', 'Franco Malone', 20, 50),
	(2, 'Bears', 'Taran William', 7, 20),
	(3, 'Great Apes', 'Becky Nicholson', 8, 24)

insert into 
	Animal(AId, Species, Name, Gender, Age, DId) 
values 
	(1, 'Bengal tiger', 'Felix', 'male', 7, 1),
	(2, 'Lion', 'Anna', 'Female', 12, 1),
	(3, 'Lion', 'Antoine', 'Male', 15, 1),
	(4, 'Jaguar', 'Peter', 'Male', 4, 1),
	(5, 'African leopard', 'Anabelle', 'Female', 9, 1),
	(6, 'Snow leopard', 'Axel', 'Male', 20, 1),
	(7, 'Southeast African cheetah', 'Bing', 'Female', 6, 1),
	(8, 'Snow leopard', 'Jake', 'Male', 3, 1)

insert into
	Animal(AId, Species, Name, Gender, Age, DId) 
values
	(9, 'Brown bear', 'Max', 'Male', 12, 2),
	(10, 'Polar bear', 'Lorena', 'Female', 1, 2)

insert into 
	Food(FId, Name, Kilos, PricePerKilo) 
values 
	('Beef', 121, 3.91),
	('Blood', 73, 270),
	('Horse meat', 45, 0.8),
	('Rabbit meat', 57, 4.27)
-- statement that violates a referential integrity constraint - foreign key AId doesn't exist
insert into 
	AnimalFood(AId, FId, Month, Year) 
values 
	(163225, 1, 'January', 2020)

insert into 
	AnimalFood(AId, FId, Month, Year) 
values 
	(1, 2, 'January', 2020),
	(2, 3, 'April', 2020),
	(3, 4, 'March', 2020),
	(4, 1, 'January', 2020),
	(4, 4, 'January', 2020),
	(5, 1, 'October', 2020),
	(5, 2, 'October', 2020),
	(6, 3, 'February', 2020),
	(6, 4, 'February', 2020),
	(7, 2, 'January', 2020),
	(7, 3, 'January', 2020)

select * from Department
select * from Animal
select * from Food
select * from AnimalFood


-- update data - for at least 3 tables
-- In the UPDATE / DELETE statements, use at least once: {AND, OR, NOT} 
-- (U1 aka Update 1), {<, <=, =, >, >=, !=} (U4), IS [NOT] NULL (U1), IN (U4), 
-- BETWEEN (U3), LIKE (U1).
update Animal
	set Name='Fernando' 
	where Name like 'Fel%' and Gender is not null
-- 'm%' is not case sensitive <=> m% same as M%
update Animal
	set Gender='Male' 
	where Gender like 'm%'

update Food
	set Kilos=131 
	where Kilos between 90 and 121

update AnimalFood
	set FId = 3 
	where AId in (1,3) and Fid != 2

update Department
	set Director='Taran William' 
	where Name='Bears'


-- delete data - for at least 2 tables
delete from Department
	where Name like '%Ape%'

delete from Food
	where PricePerKilo >= 20


-- a. 2 queries with the union operation; use UNION [ALL] and OR
select *
	from Animal A 
	where A.Age > 14
union -- works like an or; add <all> to include duplicates if they exist
select *
	from Animal A
	where A.Gender='Female'

select * 
	from Department D
	where D.NoOfAnimals > 10 or D.NoOfSpecies > 8


-- b. 2 queries with the intersection operation; use INTERSECT and IN
select distinct A.Name, A.Species, A.Gender
	from Animal A
	where A.Name like 'A_%'
intersect -- works like an add
select A.Name, A.Species, A.Gender
	from Animal A
	where A.Gender like 'F%' and (A.Species like 'L%' or A.Species like 'A%')

select distinct A.Name, A.Age
	from Animal A
	where A.Age in (10, 12, 20)


-- c. 2 queries with the difference operation; use EXCEPT and NOT IN
select *, F.Kilos*F.PricePerKilo as TotalPrice
	from Food F
	where F.Kilos > 50
except -- in 1st but not in 2nd select
select *, F.FId
	from Food F
	where F.PricePerKilo < 3

select A.Name, A.Age + 2 as AgeIn2Years
	from Animal A
	where A.Age not in (10,11,12,14)


-- d. 4 queries with INNER JOIN, LEFT JOIN, RIGHT JOIN, and FULL JOIN (one
-- query per operator); one query will join at least 3 tables, while another
-- one will join at least two many-to-many relationships
-- 3 tables
select top 6 * 
	from Department D inner join Animal A on D.DId = A.DId 
	inner join AnimalFood AF on AF.AId = A.AId
	order by A.Species
	
-- 2 many-to-many relationships
select distinct top 5 *
	from Department D left join Animal A on D.DId = A.AId
	left join AnimalFood AF on A.AId = AF.AId 
	left join Food F on AF.FId = F.FId
	order by A.Age

select D.Name, A.Species, A.Name
	from Department D right join Animal A
	on D.DId = A.DId

select *
	from AnimalFood AF full join Food F
	on AF.FId = F.FId


-- e. 2 queries using the IN operator to introduce a subquery in the WHERE
-- clause; in at least one query, the subquery should include a subquery in
-- its own WHERE clause
-- get animals which are older than 7 years and are part of departments with
-- more than 10 species
select A.Species, A.Name, A.Age*2 as DoubledAge
	from Animal A
	where A.Age > 7 and A.DId in (select D.DId from Department D
											   where D.NoOfSpecies > 10)

-- get animals which are females and are part of departments which have 'cats'
-- in their names
select *
	from Animal A
	where A.Gender like 'F%' and A.DId in (select D.DId from Department D
														where D.Name like '%cats%')


-- f. 2 queries using the EXISTS operator to introduce a subquery in the
-- WHERE clause
-- animals older than 10 years and which are part of the Franco's departments
select A.Species, A.Name, A.Age
	from Animal A
	where A.Age > 10 and exists (select D.DId from Department D
											  where A.DId = D.DId and D.Director like 'Franco%')

-- animals that have no record in AnimalFood table in 2020
select *
	from Animal A
	where not exists (select * from AnimalFood AF
							   where A.AId = AF.AId and AF.Year = 2020)


-- g. 2 queries with a subquery in the FROM clause
-- animals older than 4 years
select Z.Name, Z.Gender
	from (select A.Name, A.Gender, A.Age from Animal A
										 where A.Age > 4) Z

-- animals that are older than 15 years in cats departments
select Z.Name, Z.Species
	from (select D.Name, A.Species from Department D, Animal A
								   where D.Name like '%cats%' and A.DId = D.DId and A.Age > 15) Z


-- h. 4 queries with the GROUP BY clause, 3 of which also contain the
-- HAVING clause; 2 of the latter will also have a subquery in the HAVING
-- clause; use the aggregation operators: COUNT, SUM, AVG, MIN, MAX
-- department ids which have no animals in it
select D.DId
	from Department D inner join Animal A
	on D.DId = A.DId
	group by D.DId

-- d. ids in which the animals avg age is more than 10 and in which
-- there are at least 5 animals
select D.DId
	from Department D inner join Animal A 
	on D.DId = A.DId
	group by D.DId
	having avg(A.Age) > 10 or count(A.AId) > 4

-- d. ids in which the animals avg age is more than the min. age
-- or in which the sum of all animals age is higher than 50
select D.DId
	from Department D inner join Animal A 
	on D.DId = A.DId
	group by D.DId
	having avg(A.Age) > (select distinct min(A.Age) from Animal)
		or sum(A.Age) > 50

-- d. ids in which the max. age is lower than 20
select D.DId
	from Department D inner join Animal A 
	on D.DId = A.DId
	group by D.DId
	having (select distinct max(A.Age) from Animal) < 20


-- i. 4 queries using ANY and ALL to introduce a subquery in the WHERE
-- clause; rewrite 2 of them with aggregation operators, and the other 2
-- with [NOT] IN
-- animals that have more years than the max. age of the animals
-- from Taran's department(s)
select A.Name, A.Age
	from Animal A
	where A.Age > all(select A1.Age from Animal A1, Department D
								   where A1.DId = D.DId and D.Director like '%Taran%')
-- rewritten
select A.Name, A.Age
	from Animal A
	where A.Age > (select max(A1.Age) from Animal A1, Department D
								      where A1.DId = D.DId and D.Director like '%Taran%')

-- animals that have less years than the min. age of cats department
select A.Name, A.Age, A.DId
	from Animal A
	where A.Age < all(select A1.Age from Animal A1, Department D
									where A1.DId = D.DId and D.Name like '%cat%')
-- rewritten
select A.Name, A.Age, A.DId
	from Animal A
	where A.Age < (select min(A1.Age) from Animal A1, Department D
									  where A1.DId = D.DId and D.Name like '%cat%')

-- animals that have their age different than the ages of the animals
-- from cats departments
select A.Name, A.Age
	from Animal A
	where A.Age <> all(select A1.Age from Animal A1, Department D
									where A1.DId = D.DId and D.Name like '%cats%')
-- rewritten
select A.Name, A.Age
	from Animal A
	where A.Age not in (select A1.Age from Animal A1, Department D
									where A1.DId = D.DId and D.Name like '%cats%')

-- animals that have their age equal to one of the animals from bear department
-- and are not from that department
select A.Name, A.Age, A.Species
	from Animal A
	where A.Age = any(select A1.Age from Animal A1, Department D
									where A.AId != A1.AId and A1.DId = D.DId and D.Name like '%bear%')
-- rewritten
select A.Name, A.Age, A.Species
	from Animal A
	where A.Age in (select A1.Age from Animal A1, Department D -- equivalent with =any
									where A.AId != A1.AId and A1.DId = D.DId and D.Name like '%bear%')


-- You must use:
-- arithmetic expressions in the SELECT clause in at least 3 queries
-- conditions with AND, OR, NOT, and parentheses in the WHERE
-- clause in at least 3 queries
-- DISTINCT in at least 3 queries, ORDER BY in at least 2 queries, and
-- TOP in at least 2 queries