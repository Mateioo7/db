create database Zoo
go
use Zoo
go

create table Department
(
	DId int primary key,
	Name varchar(50),
	Director varchar(50) not null,
	NoOfSpecies int,
	NoOfAnimals int
)

create table Animal
(
	AId int primary key,
	DId int foreign key references Department(DId),
	Species varchar(50) not null,
	Name varchar(50),
	Gender varchar(10) not null,
	Age int not null
)

create table AnimalFood
(
	AId int foreign key references Animal(AId),
	FId int foreign key references Food(FId),
	Month varchar(50),
	Year int,
	constraint pk_AnimalFood primary key (AId, FId)
)

CREATE TABLE Food
(
	FId INT PRIMARY KEY IDENTITY,
	Name varchar(50),
	Kilos int not null,
	PricePerKilo float
)

create table Client
(
	CId int primary key,
	DId int foreign key references Department(DId),
	Name varchar(50),
	Surname varchar(50),
	PhoneNo int not null
)

create table Ticket
(
	TId int primary key,
	Type varchar(50),
	ActualPrice int not null,
	StandardPrice int,
	Discount int
)

create table ClientTicket
(
	CId int foreign key references Client(CId),
	TId int foreign key references Ticket(TId),
	NoOfTickets int,
	constraint pk_ClientTicket primary key (CId, TId)
)

create table DepartmentEmployee
(
	DId int foreign key references Department(DId),
	EId int foreign key references Employee(EId),
	constraint pk_DepartmentEmployee primary key (DId, EId)
)

create table Employee
(
	EId int primary key,
	SId int foreign key references Salary(SId),
	JobName varchar(50),
	Name varchar(50),
	Surname varchar(50),
	PhoneNo int not null
)

create table Salary
(
	SId int primary key,
	StandardValue int not null,
	OverworkValue int,
)
