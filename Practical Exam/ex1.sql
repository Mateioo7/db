use [Practical_Exam]
-- ex1 from drive

-- 1 drive
create table Customer (
	Id int primary key,
	Name varchar(50),
	DOB date,
)

create table BankAccount (
	IBAN int primary key,
	Balance int,
	Holder varchar(50),
)

create table CustomerBanks (
	Cid int foreign key references Customer(Id),
	Bid int foreign key references BankAccount(IBAN),
	constraint pk_CustomerBanks primary key (Cid, Bid)
)

create table Card (
	Id int primary key,
	CVV int,
	Bid int foreign key references BankAccount(IBAN)
)

create table ATM (
	Id int primary key,
	Address varchar(50)
)

create table Transaction (
	ATMid int foreign key references ATM (Id),
	Cid int foreign key references Card (Id),
	Sum int,
	constraint pk_Transaction primary key (ATMid, Cid)
)

select * from BankAccount
select * from Card
select * from [Transaction]

insert into BankAccount values (22, 5999, 'Sergiu')

insert into Card values (1, 1241, 22)
insert into Card values (2, 11, 22)

insert into ATM values (1, 'fafafaggeg')
insert into ATM values (2, 'faeg')


insert into [Transaction] values (1, 1, 2000)
insert into [Transaction] values (2, 1, 2000)
insert into [Transaction] values (2, 2, 2100)


create or alter proc deleteFromCard @id int
as
	delete from [Transaction] where Cid = @id

exec deleteFromCard 1



create or alter view allATMCards
as
	select T.Cid, count(T.ATMid) as ATMsUsed
		from [Transaction] T
		group by T.Cid
		having count(T.ATMid) = (select count(*) from ATM)

select * from allATMCards



create or alter function cardsWith2000()
returns table
as return
	select T.Cid, C.CVV, sum(T.Sum) as TransactionsSum
		from [Transaction] T inner join Card C on T.Cid = C.Id
		group by T.Cid, C.CVV
		having sum(T.Sum) > 2000

select * from cardsWith2000()