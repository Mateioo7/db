-- model_practic_emilia

/*
1. c
1
2
9

5
6 sum is 7 --WRONG

7
8

10
12

2. c

select I:
3
6
9
12
13
select II:
1
2
3
4
6
8
12

3-3 (only one 3-3)
6-6
12-12

3. b
DECLARE @no INT = 0
SELECT @no = AVG(d.M3 - i.M3)
FROM deleted d INNER JOIN inserted i ON d.PK1 = i.PK1 AND d.PK2 = i.PK2 WHERE d.M3 > i.M3
PRINT @no

d  i
15 3
8  3
10 3
6  3
20 3
15 3
15 3
10 3
____(+)
99 24

(99-24)/8 = 75/8 = 9


UPDATE M
SET M3 = 3
WHERE PK1 > PK2
*/