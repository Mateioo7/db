drop table R

create table R
(
	Rid int primary key,
	A1 varchar(50),
	K2 int,
	A2 int,
	A3 int,
	A4 int,
	A5 varchar(5),
	A6 int
)

insert into R values 
	(2, 'ada', 100, 1, 3, 3, 'M1', 22),
	(3, 'adfad', 200, 1, 3, 3, 'M1', 22),
	(4, 'fafafa', 150, 2, 3, 4, 'M1', 23),
	(5, 'dadada', 700, 2, 4, 4, 'M2', 29),
	(6, 'affa', 300, 3, 4, 5, 'M2', 29),
	(7, 'fafafa', 350, 3, 4, 5, 'M5', 23),
	(8, 'fafaffa', 400, 3, 5, 7, 'M5', 29),
	(9, 'fafafa', 500, 4, 5, 7, 'M2', 30),
	(10, 'fafafa', 450, 4, 5, 7, 'M7', 30),
	(11, 'fafaaa', 250, 4, 6, 7, 'M7', 30),
	(12, 'faaa', 800, 5, 6, 7, 'M6', 22),
	(13, 'fafa', 750, 5, 6, 7, 'M6', 23)

select * from R

SELECT r1.RID, r1.K2, COUNT(*) NumRows
FROM R r1 INNER JOIN R r2 ON r1.A2 = r2.A3
 INNER JOIN R r3 ON r2.A3 = r3.A4
WHERE r1.A1 LIKE '_%'
GROUP BY r1.RID, r1.K2
HAVING COUNT(*) >= 6
-- 1. a + b

select * FROM R r1 INNER JOIN R r2 ON r1.A2 = r2.A3
 INNER JOIN R r3 ON r2.A3 = r3.A4



---
 select distinct R.A1, R.A3 from R

SELECT r1.A6, MAX(r1.A2) MaxA2
FROM R r1
WHERE r1.A5 IN ('M1', 'M2')
GROUP BY r1.A6
EXCEPT
SELECT DISTINCT r2.A6, r2.A2
FROM R r2
-- 2. e

------------
drop table InsertLog
create table InsertLog (
	A5Value varchar(5),
	NumRows int,
	DateTimeOp date
)


CREATE OR ALTER TRIGGER TrOnInsert
 ON R
 FOR INSERT
AS
 INSERT InsertLog(A5Value, NumRows, DateTimeOp)
 SELECT i.A5, 2, GETDATE()
 FROM inserted i

 INSERT R(RID, K2, A5) VALUES
 (14, 14, 'M1'), (15, 15, 'M1'), (16, 16, 'M2')
INSERT R(RID, K2, A5) VALUES
 (17, 17, 'M1'), (18, 18, 'M3') select * from InsertLog-- 3. a-- IIdrop table DroneManufacturercreate table DroneManufacturer (	Id int primary key,	Name varchar(50),)drop table Dronecreate table Drone (	Id int primary key,	ModelId int foreign key references DroneModel (Id))drop table DroneModelcreate table DroneModel (	Id int primary key,	ManufacturerId int foreign key references DroneManufacturer(Id),	Name varchar(50),	BatteryLife int,	MaxSpeed int)create table PizzaShop (	Name varchar(50),	Address varchar(100)	constraint uk_PizzaShopName unique(Name))create table Customer (	Name varchar(50),	LoyaltyScore int,	constraint uk_CustomerName unique(Name))drop table Deliverycreate table Delivery (	CName varchar(50) foreign key references Customer(Name),	PSName varchar(50) foreign key references PizzaShop(Name),	Did int foreign key references Drone(Id),	Date datetime	constraint pk_Delivery primary key(CName, PSName, Did))-- bcreate or alter proc addDelivery @c varchar(50), @ps varchar(50), @d int, @dt datetimeas	declare @nr int = 0	select @nr = count(*) from Delivery D 		where D.Cname = @c and D.PSName = @ps and D.Did = @d	if (@nr != 0) begin		update Delivery		set Date = @dt		where Cname = @c and PSName = @ps and Did = @d	end	else begin		insert into Delivery values (@c, @ps, @d, @dt)	endinsert into Customer values ('Sergiu', 34)insert into PizzaShop values ('SebastianPizza', 'Street 2, building 4')insert into Drone values (10, 1)select * from Droneselect * from PizzaShopexec addDelivery 'Sergiu', 'SebastianPizza', 10, '01/01/2011 23:59:59.999'select * from Delivery-- cinsert into DroneManufacturer values (1, 'Manufacturer1')insert into DroneModel values (1, 1, 'Model1', 750, 30)create or alter view favDMas	select DM.Id, DM.Name, count(*) as DronesUsed	from DroneManufacturer DM inner join DroneModel DMo on DM.Id = DMo.ManufacturerId	inner join Drone Dr on Dr.ModelId = DMo.Id	inner join Delivery D on D.Did = Dr.Id	group by DM.Id, DM.Name	having count(*) = (select max(x.TimesUsed) from (select count(Did) as TimesUsed from Delivery								group by Did) x)select * from favDM-- dcreate or alter function customersWithDeliveries(@d int)returns tableas return	select D.CName, count(D.Cname) as NoOfDeliveries	from Delivery D	group by D.Cname	having count(D.Cname) >= @dselect * from customersWithDeliveries(2)select * from Deliveryinsert into Customer values ('David', 56)insert into PizzaShop values ('Mondo', 'Street 34, building 2')insert into Drone values (12, 2)insert into DroneManufacturer values (2, 'Manufacturer2')insert into DroneModel values (2, 2, 'Model2', 750, 30)exec addDelivery 'David', 'Mondo', 12, '01/01/2015 23:59:59.999'select * from Deliveryexec addDelivery 'Sergiu', 'Mondo', 12, '01/01/2016 23:59:59.999'exec addDelivery 'David', 'Mondo', 10, '01/01/2016 23:59:59.999'