use [Practical_Exam]

create table PresentationShop (
	Id int primary key,
	Name varchar(50),
	City varchar(50)
)

create table Woman (
	Id int primary key,
	Name varchar(50),
	MaxToSpend int
)

create table ShoeModel (
	Id int primary key,
	Name varchar(50),
	Season varchar(50),
)

create table Shoe (
	Id int primary key,
	Price int,
	Model int foreign key references ShoeModel(Id)
)

create table PresentationShoes (
	Pid int foreign key references PresentationShop(Id),
	Sid int foreign key references Shoe(Id),
	AvailableShoes int
)

create table WomenShoes (
	Wid int foreign key references Woman(Id),
	Sid int foreign key references Shoe(Id),
	NoOfShoes int,
	MoneySpent int
)

insert into PresentationShop values (1, 'Angelo', 'fafafaa'),
(2, 'Simon', 'dafaggs')

insert into ShoeModel values (1, 'Model1', 'Season1'),
(2, 'Model2', 'Season2')

insert into Shoe values (1, 200, 1), (2, 300, 2)
insert into Shoe values (3, 100, 1)


create or alter proc addShoe @sid int, @psid int, @sn int
as
	declare @nr int = 0	select @nr = count(*) from PresentationShoes PS		where PS.Pid = @psid and PS.Sid = @sid	if (@nr != 0) begin		update PresentationShoes		set AvailableShoes = @sn		where Pid = @psid and Sid = @sid	end	else begin		insert into PresentationShoes values (@psid, @sid, @sn)	end

exec addShoe 1, 1, 4

exec addShoe 2, 2, 5

select * from PresentationShoes





insert into Woman values (1, 'Ana', 400), (2, 'Ale', 200)

insert into WomenShoes values (1, 1, 1, 200), (2, 2, 2, 150)
insert into WomenShoes values (1, 3, 1, 200)
insert into WomenShoes values (2, 3, 3, 100)
insert into WomenShoes values (2, 1, 1, 100)

select * from WomenShoes

drop view WomenWithMoreThan2ShoesOfModel
create or alter view WomenWithMoreThan1ShoeOfModel
as
	select W.Id, W.Name, count(*) as ShoesBoughtOfModel
	from WomenShoes WS inner join Woman W on WS.Wid = W.Id
	inner join Shoe S on WS.Sid = S.Id
	group by S.Model, W.Id, W.Name
	having count(*) >= 2


select * from WomenWithMoreThan1ShoeOfModel



select * from PresentationShop
select * from PresentationShoes

insert into PresentationShoes values (2, 1, 6)



create or alter function shoesInShops(@T int)
returns table
as return
	select S.Id, count(*) as NoOfShops
	from PresentationShoes PS inner join Shoe S on PS.Sid = S.Id
	group by S.Id, S.Price
	having count(*) >= @T

select * from shoesInShops(2)